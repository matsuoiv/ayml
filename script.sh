#!/usr/bin/env bash

set -e

DOMAIN="redo.local"
IMAGE_NAME="php-symfony"

IMAGE_CLI="matsuoiv/php-symfony:cli"

FRONTEND_DIR=app/frontend
CK='\u2714'
ER='\u274c'
# DIRECTORY_VARIABLE='/usr/local/etc/variable.env'

alias cls='printf "\033c"'

export DEV_UID=$(id -u)
export DEV_GID=$(id -g)
# export $(egrep -v '^#' $DIRECTORY_VARIABLE | xargs)

access_aws_credentials()
{
    docker run \
    --env AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID \
    --env AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY \
    garland/aws-cli-docker \
    aws $@

}

app_save_acces_credential_aws()
{
    read -p "Enter AWS ACCESS KEY: " AWS_ACCESS_KEY_ID
    read -p "Enter AWS SECRET KEY: " AWS_SECRET_ACCESS_KEY
    echo -e "AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID\nAWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY" > "$DIRECTORY_VARIABLE"
}

app_start()
{
    docker-compose up -d

    if [ $? -eq 0 ]; then
        echo -e "\n\n$CK  [Docker UP] "
        echo -e "\n----------------------------------------------------------"
        echo -e "\n App Server RUN  ===> http://$DOMAIN   \r"
        echo -e "\n----------------------------------------------------------\n"
    else
        echo -e "\n$ER [Docker UP] No se pudo levantar docker.\n"
    fi

}

app_down()
{
   docker-compose down

   echo -e "\n\n$CK  [Docker Down] \n"
}

app_docker_images_build()
{
   docker-compose -f docker-compose.build.yml build $@
}

app_docker_images_push()
{
   docker push docker.orbis.pe/$IMAGE:base && \
   docker push docker.orbis.pe/$IMAGE:nginx && \
   docker push docker.orbis.pe/$IMAGE:cli
}

app_docker_images_pull()
{
   docker pull docker.orbis.pe/$IMAGE:base && \
   docker pull docker.orbis.pe/$IMAGE:nginx && \
   docker pull docker.orbis.pe/$IMAGE:cli
}

app_composer_cmd()
{
    docker run -i -v $PWD/app:/app/ -v $PWD/.composer/:/tmp/.composer/ $IMAGE_CLI composer $@
}

app_npm_cmd()
{
    export CDPATH=
    wd=$PWD

    cd $FRONTEND_DIR

    npm $@

    cd "$wd"
    echo -e "\n\n$CK  [Node Script] $@"
}

app_yarn_cmd()
{
    mkdir -p $FRONTEND_DIR/.config/yarn/global
    mkdir -p $FRONTEND_DIR/.config/yarn/link
    mkdir -p $FRONTEND_DIR/.cache/yarn/v1

    docker-compose -f docker-compose.tasks.yml run --rm yarn yarn $@

    echo -e "\n\n$CK  [Node Script] $@"
}

command_exists ()
{
    type "$1" &> /dev/null ;
}

app_remove_var_cmd(){
    sudo rm -rf ./app/var/cache && sudo rm -rf ./app/var/logs
    mkdir ./app/var/cache && mkdir ./app/var/cache/test && mkdir ./app/var/cache/dev
    sudo chmod 777 -R ./app/var/
}

app_test_cmd()
{
    app_remove_var_cmd
    docker-compose -f docker-compose.tests.yml down \
    && docker-compose -f docker-compose.tests.yml run --rm test
    chmod 777 -R ./app/var
}

app_install()
{
   echo -e "\r"

   if ! grep -q "$DOMAIN" /etc/hosts;
    then
      echo -e "\nSetting Virtualhost ....\n"
      sudo su -c "echo '127.0.0.1 $DOMAIN' >> /etc/hosts"
      if [ $? -eq 0 ]; then
          echo -e "$CK  [Virtualhost] "
      else
         echo -e "\r $ER [Virtualhost] Error al configurar el virtualhost."
         exit
      fi
   else
        echo -e "$CK  [Virtualhost] "
   fi

   if [ ! -d "./app/composer.lock" ]; then
       echo -e "\nInstall dependencies .... "
       app_composer_cmd install \
          --no-progress \
          --profile \
          --prefer-dist
       if [ $? -eq 0 ]; then
         echo -e "$CK  [Dependencies] "
       else
         echo -e "\r $ER [Dependencies] Ocurrio un error al instalar las dependencias"
         exit
       fi
   else
      app_composer_cmd update \
          --no-progress \
          --profile \
          --prefer-dist

      echo -e "$CK  [Dependencies] "
   fi

   echo -e "\n--------------------------------------------------"
   echo -e "  [OK] Ejecutar make start o docker-compose up   "
   echo -e "\n  Go to ==> http://$DOMAIN   \r"
   echo -e "----------------------------------------------------\n"
}

app_symfony_console()
{
    docker-compose exec --user $(id -u):$(id -g) php bin/console $@
}

case "$1" in
"install")
    app_install
    ;;
"start")
    app_start
    ;;
"stop")
    app_down
    ;;
"console")
    app_symfony_console ${@:2}
    ;;
"composer")
    app_composer_cmd ${@:2}
    ;;
"npm")
    app_npm_cmd ${@:2}
    ;;
"build")
    app_docker_images_build ${@:2}
    ;;
"pull")
    app_docker_images_pull
    ;;
"push")
    app_docker_images_push
    ;;
"test")
    app_test_cmd
    ;;
*)
    echo -e "\n\n\n$ER [APP] No se especifico un comando valido\n"
    ;;
esac