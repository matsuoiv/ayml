# Microservice Institutions

## Quick Configuration

#### Installing the AWS Command Line Interface

```
pip install --upgrade pip
pip install awscli --upgrade --user
```

#### Log in to Amazon ECR registry

```
make login
```

Enter your AWS account credentials

```
AWS Access Key ID: EnterYourAccessKeyID
AWS Secret Access Key: EnterYoutSecretAccessKey
```

## Install project

```
make install
```

## Run project

```
make start
```

### Go to:

http://local.services.aptitus.com/v1/institutions/doc

## Option make

```
Option
build            build image, usage: make build, make build image=nginx
cache            Pull all images from registry
clean            Clear containers
clear-cache-app  Clear cache application
composer         Install composer dependency, usage: make composer req=symfony/dotenv
composer-update  Update composer dependencies
gulp             Run gulp task
install          Install project
login            Login aws
pull             Pull all images from registry
push             Push all images to registry
restart          Restart all containers, usage: make restart
ssh              Enter ssh container, usage : make ssh container=nginx
start            Up docker containers, usage: make up
status           Show containers status, usage: make status
stop             Stops and removes the docker containers, usage: make down
test             Execute integration test
```

example: `make [option]`